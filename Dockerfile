FROM node:9.11.2-alpine
RUN mkdir -p /code
WORKDIR /code
ADD . /code
RUN npm set registry https://npm-registry.surgeforward.com
RUN npm install -g -s --no-progress yarn && \
    yarn install
CMD [ "yarn", "run", "storybook" ]
EXPOSE 6006