## Importing

``` javascript
import TimezoneSelector from '@surge/react-timezone-selector';
```

## react-timezone-selector

The plugin can be used like so:

``` javascript
import TimezoneSelector from '@surge/react-timezone-selector';

const onTimezoneSelected = (timezone) => {
  console.log(timezone);
}

const selectedTimezone = 'America/New_York';
...
render = () => <TimezoneSelector onTimezoneSelected={onTimezoneSelected} selectedTimezone={selectedTimezone} />
```

The props needed are:

#### onTimezoneSelected: an output function that will be passed the currently selected timezone

#### selectedTimezone: the pre-selected timezone when the component is mounted
ex.
``` javascript
    'America/New_York'
```