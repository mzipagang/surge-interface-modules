import React from "react";
import PropTypes from 'prop-types';
import styled from "styled-components";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faChevronDown
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Select from 'react-select';

library.add(faChevronDown);

const Container = styled.div`
  position: relative;
`;

const maxDropdownHeight = 600;

const filterByKeyword = (timezones, keyword) =>
  timezones.filter(
    timezone => (timezone && timezone.toLocaleLowerCase().includes(keyword.toLocaleLowerCase()))
     || keyword === ""
  );

export default class TimezonesDropdown extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedTimezone: props.selectedTimezone,
      keyword: ""
    };
  }

  onTimezoneSelected = (timezone) => {
    this.setState({ selectedTimezone: timezone });
    this.props.onTimezoneSelected(timezone);
  }

  enterTimezone = (e) => {
    this.setState({ keyword: e.target.value });
  }

  render = () => {
    const timezones = filterByKeyword(this.props.timezones || [], this.state.keyword)
      .map(timezone => ({value: timezone, label: timezone}));

    return (
      <Container>
        <Select maxMenuHeight={maxDropdownHeight}
          options={timezones}
          value={timezones.find(s => s.value === this.state.selectedTimezone)}
          onChange={(option) => { this.onTimezoneSelected(option.value) }}
          classNamePrefix="react-select" />
      </Container>
    );
  }
};

TimezonesDropdown.propTypes = {
  timezones: PropTypes.array,
  onTimezoneSelected: PropTypes.func
};
