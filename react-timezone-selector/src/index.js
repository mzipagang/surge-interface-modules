import React from 'react';
import moment from 'moment-timezone'
import TimezonesDropdown from "./components/TimezonesDropdown.js"

const timezones = moment.tz.names();
const TimezoneSelector = (props) => <TimezonesDropdown timezones={timezones} {...props} />

export default TimezoneSelector;