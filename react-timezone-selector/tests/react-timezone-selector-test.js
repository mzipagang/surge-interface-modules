import expect from 'expect';
import React from 'react';
import ReactDOM from 'react-dom';
import ReactTestUtils from 'react-dom/test-utils';
import TimezoneSelector from '../src';

class Wrapper extends React.PureComponent {
  render() {
    return <TimezoneSelector selectedTimezone={'America/New_York'} />;
  }
}

describe("TimezoneSelector", () => {
  it("renders the pre-selected timezone in the dropdown", function() {  
    this.component = ReactTestUtils.renderIntoDocument(<Wrapper />);

    const input = ReactTestUtils.findRenderedDOMComponentWithClass(this.component, "react-select__single-value");
    expect(input.innerHTML).toEqual('America/New_York');
  });
});