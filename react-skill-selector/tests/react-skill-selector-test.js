import expect from 'expect';
import React from 'react';
import ReactDOM from 'react-dom';
import ReactTestUtils from 'react-dom/test-utils';
import SkillSelector from '../src';

describe("SkillSelector", () => {
  it("renders a pre-selected skills list", function() {    
    this.component = ReactTestUtils.renderIntoDocument(
      <SkillSelector
        selectedSkills={[
          { name: "mysql", competency: { rating: 4, text: "Proficient" } },
          { name: "c#", competency: { rating: 5, text: "Expert" } },
        ]}
        savedSearches={[]}
        skills={[
          { name: "mysql", numRatings: 5 },
          { name: "c#", numRatings: 0 },
          { name: "javascript" }
        ]}
        competencyList={[
          { rating: 1, text: "Novice" },
          { rating: 2, text: "Advanced Beginner" },
          { rating: 3, text: "Competent" },
          { rating: 4, text: "Proficient" },
          { rating: 5, text: "Expert" },
          { rating: null, text: "Nice to Have" }
        ]}
        defaultCompetencyIndex={2}
      />
    );

    const list = ReactTestUtils.scryRenderedDOMComponentsWithTag(this.component, "li");
    expect(list.length).toEqual(2);
    expect(list[0].innerHTML).toContain('mysql');
    expect(list[0].innerHTML).toNotContain('javascript');
    expect(list[1].innerHTML).toContain('c#');
    expect(list[1].innerHTML).toNotContain('javascript');

    this.renderedDOM = () => ReactDOM.findDOMNode(this.component);

    const rootElement = this.renderedDOM();
    expect(rootElement.innerHTML).toNotContain("Saved Searches");
  });

  it("renders no save feature if enableSave is false", function() {    
    this.component = ReactTestUtils.renderIntoDocument(
      <SkillSelector
        enableSave={false}
      />
    );

    this.renderedDOM = () => ReactDOM.findDOMNode(this.component);

    const rootElement = this.renderedDOM();
    expect(rootElement.innerHTML).toNotContain("Saved Searches");
  });

  it("renders save feature if enableSave is true", function() {    
    this.component = ReactTestUtils.renderIntoDocument(
      <SkillSelector
        enableSave={true}
      />
    );

    this.renderedDOM = () => ReactDOM.findDOMNode(this.component);
    
    const rootElement = this.renderedDOM();
    expect(rootElement.innerHTML).toContain("Saved Searches");
  });
});