import React from "react";
import PropTypes from 'prop-types';
import styled from "styled-components";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faSave,
  faTimesCircle,
  faSearch,
  faTrashAlt,
  faChevronDown
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import Modal from "react-awesome-modal";

import SkillsDropdown from "./components/SkillsDropdown.js";
import SkillCompetencies from "./components/SkillCompetencies.js";
import SearchControls from "./components/SearchControls.js";
import SavedSearches from "./components/SavedSearches.js";

library.add(faSave, faTimesCircle, faSearch, faTrashAlt, faChevronDown);

const ModalHeader = styled.div`
  display: flex;
  -ms-flex-align: start;
  align-items: flex-start;
  -ms-flex-pack: justify;
  justify-content: space-between;
  padding: 1rem;
  font-size: 18px;
  border-bottom: 1px solid #eee;
`;

const ModalClose = styled.button`
  font-size: 18px;
  border: none;
  background: none;
  cursor: pointer;
`;

const ModalBody = styled.div`
  position: relative;
  -ms-flex: 1 1 auto;
  flex: 1 1 auto;
  padding: 1rem;
`;

const ModalInput = styled.input`
  margin: 5px 3px;
  padding: 10px 0;
  outline: none;
  width: 98%;
  border: none;
  border-bottom: 1px solid #eee;
  font-size: 16px;
`;

const ModalSaveButton = styled.button`
  padding: 15px;
  margin: 10px 0;
  width: 100%;
  border-radius: 5px;
  border: none;
  background: #0d87e9;
  color: white;
  text-transform: uppercase;
`;

const serialize = (skills) =>
  (skills || []).map(
    skill => skill ? (`${skill.name}` + (skill.competency ? `-${skill.competency.rating}` : '')) : ''
  ).join(',');

const onSelectedSkillsChanged = (props, skills) => {
  if (props.onSelectedSkillsChanged) {
    props.onSelectedSkillsChanged(skills);
  }
}

const onSavedSearchesChanged = (props, savedSearches) => {
  if (props.onSavedSearchesChanged) {
    props.onSavedSearchesChanged(savedSearches);
  }
}

export default class SkillSelector extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      skills: [],
      selectedSkills: [],
      competencyList: [],
      defaultCompetencyIndex: 0,
      savedSearches: [],
      currentSavedSearch: {},
      saveSearchName: "",
      showSaveSearchButton: false,
      showAlert: true,
      modalVisible: false
    };
  }

  componentDidMount() {
    this.setState({
      skills: [...this.props.skills || []],
      selectedSkills: [...this.props.selectedSkills || []],
      competencyList: [...this.props.competencyList || []],
      defaultCompetencyIndex: this.props.defaultCompetencyIndex,
      savedSearches: [...this.props.savedSearches || []]
    });

    if (this.props.selectedSkills && this.props.selectedSkills.length) {
      this.setState({ showSaveSearchButton: true });
    }
  }

  static getDerivedStateFromProps(props, state) {
    let  newState = {};
    if (
      props.skills &&
      props.skills.length &&
      props.skills.length !== state.skills.length
    ) {
      newState = {...newState, ...{skills: [...props.skills]}};
    }
    if (
      props.competencyList &&
      props.competencyList.length &&
      props.competencyList.length !== state.competencyList.length
    ) {
      newState = {...newState, ...{competencyList: [...props.competencyList]}};
    }
    if (
      state.selectedSkills &&
      state.selectedSkills.length &&
      serialize(props.selectedSkills) !== serialize(state.selectedSkills)
    ) {
      newState = {...newState, ...{selectedSkills: [...state.selectedSkills]}};
    }
    return newState;
  }

  addSkill = (skill) => {
    if (
      !this.state.selectedSkills.some(
        selectedSkill => selectedSkill.name === skill.name
      )
    ) {
      skill.competency = this.state.competencyList[this.state.defaultCompetencyIndex || 0];
      const updatedSkills = [...this.state.selectedSkills, skill];
      this.setState({ selectedSkills: updatedSkills });
      this.setState({ showAlert: false });
      this.setState({ showSaveSearchButton: true });

      onSelectedSkillsChanged(this.props, updatedSkills);
    }
  }

  removeSkill = (skill) => {
    const remainingSkills = this.state.selectedSkills.filter(function(
      selectedSkill
    ) {
      return skill.name !== selectedSkill.name;
    });
    this.setState({ selectedSkills: remainingSkills });
    if (remainingSkills.length === 0) {
      this.setState({ showSaveSearchButton: false });
    }

    onSelectedSkillsChanged(this.props, remainingSkills);
  }

  selectCompetency = (skill, competency) => {
    const currentSkill = this.state.selectedSkills.find(
      selectedSkill => selectedSkill.name === skill.name
    );
    currentSkill.competency = competency;

    onSelectedSkillsChanged(this.props, this.state.selectedSkills);
  }

  saveSearch = (name) => {
    if (name) {
      const savedSearch = {
        id: Date.now(),
        name: name,
        selectedSkills: [...this.state.selectedSkills]
      };
      const savedSearches = [...this.state.savedSearches, savedSearch];
      this.setState({ currentSavedSearch: savedSearch });
      this.setState({ savedSearches: savedSearches });

      onSavedSearchesChanged(this.props, savedSearches);

      this.closeModal();
    }
  }

  enterSaveName = (e) => {
    this.setState({ saveSearchName: e.target.value });
  }

  closeSavedSearch = () => {
    this.setState({ selectedSkills: [] });
    this.setState({ showSaveSearchButton: false });

    onSelectedSkillsChanged(this.props, []);
  }

  deleteSavedSearch = (selectedSavedSearch) => {
    const savedSearches = this.state.savedSearches.filter(function(
      savedSearch
    ) {
      return !selectedSavedSearch || savedSearch.id !== selectedSavedSearch.id;
    });
    if (
      selectedSavedSearch &&
      selectedSavedSearch.id === this.state.currentSavedSearch.id
    ) {
      this.closeSavedSearch();
    }
    this.setState({ savedSearches: savedSearches });

    if (selectedSavedSearch.id) {
      onSavedSearchesChanged(this.props, savedSearches);
    }
  }

  applySavedSearch = (savedSearch) => {
    this.setState({ currentSavedSearch: { ...savedSearch } });
    const savedSkills = savedSearch.selectedSkills.map(skill => ({ ...skill }));
    this.setState({
      selectedSkills: savedSkills
    });
    this.setState({ showSaveSearchButton: true });

    onSelectedSkillsChanged(this.props, savedSkills);
  }

  openModal = () => {
    this.setState({
      modalVisible: true
    });
  }

  closeModal = () => {
    this.setState({
      modalVisible: false
    });
  }

  render() {
    return (
      <div>
        {this.props.enableSave &&
          this.state.showSaveSearchButton && (
            <div>
              <SearchControls
                currentSavedSearch={this.state.currentSavedSearch}
                saveSearch={this.openModal}
                closeSavedSearch={this.closeSavedSearch}
                deleteSavedSearch={this.deleteSavedSearch}
              />
              <Modal
                visible={this.state.modalVisible}
                width="500"
                height="198"
                effect="fadeInUp"
                onClickAway={() => this.closeModal()}
              >
                <div>
                  <ModalHeader>
                    <div>Enter Search Name</div>
                    <ModalClose onClick={() => this.closeModal()}>X</ModalClose>
                  </ModalHeader>
                  <ModalBody>
                    <ModalInput
                      placeholder="search name"
                      type="text"
                      value={this.state.saveSearchName}
                      onChange={this.enterSaveName}
                    />
                    <ModalSaveButton
                      onClick={() =>
                        this.saveSearch(
                          this.state.saveSearchName
                        )
                      }
                    >
                      <FontAwesomeIcon icon="save" /> Save Search
                    </ModalSaveButton>
                  </ModalBody>
                </div>
              </Modal>
            </div>
          )}
        {this.state.selectedSkills.length > 0 && (
          <SkillCompetencies
            selectedSkills={this.state.selectedSkills}
            removeSkill={this.removeSkill}
            selectCompetency={this.selectCompetency}
            competencyList={this.state.competencyList}
            defaultCompetency={this.state.competencyList[this.state.defaultCompetencyIndex || 0]}
          />
        )}
        <SkillsDropdown
          id={this.props.id}
          addSkill={this.addSkill}
          skills={this.state.skills}
          competencyList={this.state.competencyList}
        />
        {this.props.enableSave && (
          <SavedSearches
            savedSearches={this.state.savedSearches}
            applySavedSearch={this.applySavedSearch}
            deleteSavedSearch={this.deleteSavedSearch}
          />
        )}
      </div>
    );
  }
}

SkillSelector.propTypes = {
  onSelectedSkillsChanged: PropTypes.func,
  selectedSkills: PropTypes.array,
  onSavedSearchesChanged: PropTypes.func,
  savedSearches: PropTypes.array,
  skills: PropTypes.array,
  competencyList: PropTypes.array,
  defaultCompetencyIndex: PropTypes.number,
  enableSave: PropTypes.bool
};
