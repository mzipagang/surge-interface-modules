import React, { PureComponent } from "react";
import PropTypes from 'prop-types';
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const SavedSearchesHeader = styled.div`
  padding: 30px 9px 5px 9px;
  font-weight: bold;
  position: relative;
`;

const SearchListBox = styled.ul`
  list-style-type: none;
  padding: 5px 10px;
`;

const SearchList = styled.li`
  display: flex;
`;

const SearchButton = styled.button`
  text-transform: uppercase;
  border: none;
  box-shadow: 0 1px 4px rgba(0, 0, 0, 0.4);
  margin-left: 10px;
  color: red;
  color: ${props => (props.primary ? "white" : "red")};
  font-weight: bold;
  background: white;
  background: ${props => (props.primary ? "#0D87E9" : "white")};
  padding: 4px 10px;
  margin-bottom: 5px;
  margin-top: 2px;
  &:hover {
    color: ${props => (props.primary ? "" : "white")};
    background: ${props => (props.primary ? "" : "red")};
  }
`;

const SearchButtons = styled.div`
  margin-left: auto;
`;

export default class SavedSearches extends PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <SavedSearchesHeader>Saved Searches</SavedSearchesHeader>
        <SearchListBox>
          {this.props.savedSearches && this.props.savedSearches.length ? (
            this.props.savedSearches.map(savedSearch => (
              <SearchList key={savedSearch.id}>
                <div>{savedSearch.name}</div>
                <SearchButtons>
                  <SearchButton
                    primary
                    onClick={() => this.props.applySavedSearch(savedSearch)}>
                    <FontAwesomeIcon icon="search" />
                  </SearchButton>
                  <SearchButton
                    onClick={() => this.props.deleteSavedSearch(savedSearch)}>
                    <FontAwesomeIcon icon="trash-alt" />
                  </SearchButton>
                </SearchButtons>
              </SearchList>
            ))
          ) : (
            <li>No Saved Searches</li>
          )}
        </SearchListBox>
      </div>
    );
  }
}

SavedSearches.propTypes = {
  savedSearches: PropTypes.array,
  applySavedSearch: PropTypes.func,
  deleteSavedSearch: PropTypes.func
};
