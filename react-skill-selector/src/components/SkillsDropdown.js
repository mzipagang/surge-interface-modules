import React from "react";
import PropTypes from 'prop-types';
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Select from 'react-select';

const Container = styled.div`
  position: relative;
`;

const maxDropdownHeight = 600;

const filterByKeyword = (skills, keyword) =>
  skills.filter(
    skill => (skill.name && skill.name.toLocaleLowerCase().includes(keyword.toLocaleLowerCase())) || keyword === ""
  );

export default class SkillsDropdown extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      keyword: ""
    };
  }

  onSkillSelected = (name) => {
    const skill = this.props.skills.find(s => name === s.name);
    this.props.addSkill(skill);
  }

  enterSkill = (e) => {
    this.setState({ keyword: e.target.value });
  }

  render() {
    const skills = filterByKeyword(this.props.skills, this.state.keyword)
      .map(skill => ({value: skill.name, label: skill.numRatings > 0 ? `${skill.name} (# ratings: ${skill.numRatings})` : skill.name}));

    return (
      <Container>
        <Select maxMenuHeight={maxDropdownHeight}
          inputId={ this.props.id }
          options={skills}
          value={null}
          onChange={(option) => { this.onSkillSelected(option.value) }}
          classNamePrefix="react-select" />
      </Container>
    );
  }
}

SkillsDropdown.propTypes = {
  addSkill: PropTypes.func,
  skills: PropTypes.array,
  competencyList: PropTypes.array
};
