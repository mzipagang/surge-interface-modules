import React, { PureComponent } from "react";
import PropTypes from 'prop-types';
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const SaveSearchContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-right: 20px;
`;

const SaveSearchList = styled.ul`
  list-style-type: none;
`;

const SaveSearch = styled.button`
  text-transform: uppercase;
  border: none;
  border-radius: 2px;
  box-shadow: 0 1px 4px rgba(0, 0, 0, 0.4);
  background: white;
  padding: 5px 10px;
  font-size: 12px;
  color: blue;
  &:hover {
    color: white;
    background: blue;
  }
`;

const SearchButtonRow = styled.button`
  border: none;
  box-shadow: 0 1px 4px rgba(0, 0, 0, 0.4);
  color: ${props => (props.primary ? "#0d87e9" : "red")};
  background: white;
  padding: 4px 10px;
  margin: 2px 5px 5px
  &:hover {
    color: white;
    background: ${props => (props.primary ? "#0d87e9" : "red")};
  }
`;

export default class SearchControls extends PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <SaveSearchContainer>
          <SaveSearch onClick={() => this.props.saveSearch()}>
            <FontAwesomeIcon icon="save" /> Save Search
          </SaveSearch>
        </SaveSearchContainer>
        <SaveSearchList>
          <li>
            <SaveSearchContainer>
              <SearchButtonRow primary onClick={this.props.closeSavedSearch}>
                <FontAwesomeIcon icon="times-circle" />
              </SearchButtonRow>
              <SearchButtonRow
                onClick={() =>
                  this.props.deleteSavedSearch(this.props.currentSavedSearch)
                }
              >
                <FontAwesomeIcon icon="trash-alt" />
              </SearchButtonRow>
            </SaveSearchContainer>
          </li>
        </SaveSearchList>
      </div>
    );
  }
}

SearchControls.propTypes = {
  currentSavedSearch: PropTypes.object,
  saveSearch: PropTypes.func,
  closeSavedSearch: PropTypes.func,
  deleteSavedSearch: PropTypes.func
};