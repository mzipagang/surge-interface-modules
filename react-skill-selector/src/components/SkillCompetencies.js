import React from "react";
import PropTypes from 'prop-types';
import styled from "styled-components";
import Select from 'react-select';

const SkillList = styled.ul`
  list-style: none;
  padding-left: 10px;
`;

const SkillSet = styled.li`
  display: flex;
  flex-direction: row;
  margin-bottom: 5px;
`;

const SkillName = styled.div`
  width: 90px;
  font-weight: bold;
`;

const SkillLevel = styled.div`
  width: 100%;
`;

const SkillButton = styled.button`
  border: none;
  box-shadow: 0 1px 4px rgba(0, 0, 0, 0.4);
  margin-left: 10px;
  color: red;
  font-weight: bold;
  background: white;
  padding: 3px 10px;
  &:hover {
    color: white;
    background: red;
  }
`;

const maxDropdownHeight = 600;

export default class SkillCompetencies extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedSkills: []
    };
  }

  static getDerivedStateFromProps(props, state) {
    let  newState = {};
    newState = {...newState, ...{selectedSkills: [...props.selectedSkills]}};
    return newState;
  }

  onChange = (skill, competencyOption) => {
    const competency = { rating: competencyOption.value, text: competencyOption.label.replace(` (${competencyOption.value})`, '') };
    this.props.selectCompetency(skill, competency);
    const selected = this.state.selectedSkills.find(selectedSkill => selectedSkill.name === skill.name);
    selected.competency = competency;
    this.setState({ selectedSkills: [...this.state.selectedSkills] })
  }

  render() {
    const competencies = this.props.competencyList.map(competency =>
      ({ value: competency.rating, label: competency.rating ? competency.text + ' (' + competency.rating + ')' : competency.text }));
  
    return (
      <SkillList>
        {this.state.selectedSkills.map(skill => (
          <SkillSet key={skill.name}>
            <SkillName>{skill.name}</SkillName>
            <SkillLevel>
              <Select maxMenuHeight={maxDropdownHeight}
                options={competencies}
                value={competencies.find(c => (skill.competency || this.props.defaultCompetency || {}).rating === c.value)}
                onChange={(option) => { this.onChange(skill, option) }}
                classNamePrefix="react-select" />
            </SkillLevel>
            <SkillButton onClick={() => this.props.removeSkill(skill)}>
              -
            </SkillButton>
          </SkillSet>
        ))}
      </SkillList>
    );
  }
}

SkillCompetencies.propTypes = {
  selectedSkills: PropTypes.array,
  removeSkill: PropTypes.func,
  selectCompetency: PropTypes.func,
  competencyList: PropTypes.array,
  defaultCompetency: PropTypes.object
};