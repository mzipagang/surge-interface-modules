## Importing

``` javascript
import SkillSelector from '@surge/react-skill-selector'
```

## react-skill-selector

The plugin can be used like so:

``` javascript
import SkillSelector from '@surge/react-skill-selector';

onSelectedSkillsChanged = (skills) => {
  console.log(skills);
}

setSelectedSkills = (skills) => {
  return [
    { name: "mysql", competency: { rating: 4, text: "Proficient" } },
    { name: "c#", competency: { rating: 5, text: "Expert" } },
  ]
}

onSavedSearchesChanged = (savedSearches) => {
  console.log(savedSearches);
}

const getSkills = () => {
  return [
    { name: "mysql", numRatings: 5 },
    { name: "c#", numRatings: 0 },
    { name: "javascript" }
  ];
}

const getCompetencies = () => {
  return [
    { rating: 1, text: "Novice" },
    { rating: 2, text: "Advanced Beginner" },
    { rating: 3, text: "Competent" },
    { rating: 4, text: "Proficient" },
    { rating: 5, text: "Expert" },
    { rating: null, text: "Nice to Have" }
  ];
}
...
render = () =>
  <SkillSelector
    onSelectedSkillsChanged={onSelectedSkillsChanged}
    selectedSkills={setSelectedSkills()}
    onSavedSearchesChanged={onSavedSearchesChanged}
    savedSearches={[]}
    skills={getSkills()}
    competencyList={getCompetencies()}
    defaultCompetencyIndex={2}
    enableSave={true}
    id="testSelector"
  />
```

The props needed are:

#### onSelectedSkillsChanged: an output function that will be passed the currently selected skills

#### skills: an array to populate the skills dropdown
ex. 
``` javascript
    [
      { name: 'mysql', numRatings: 5 },
      { name: 'c#', numRatings: 0 },
      { name: 'javascript' }
    ]
```

#### selectedSkills: an array of skills pre-selected when the component is mounted
ex: 
``` javascript
    [
      { name: "mysql", competency: { rating: 4, text: "Proficient" } },
      { name: "c#", competency: { rating: 5, text: "Expert" } },
    ]
```

#### competencyList: an array of strings to populate the competency dropdown
ex.
``` javascript
    [
      { rating: 1, text: "Novice" },
      { rating: 2, text: "Advanced Beginner" },
      { rating: 3, text: "Competent" },
      { rating: 4, text: "Proficient" },
      { rating: 5, text: "Expert" },
      { rating: null, text: "Nice to Have" }    
    ]
```

#### defaultCompetencyIndex: the index of the default competency in competencyList

#### onSavedSearchesChanged: an output function that will be passed the current saved searches

#### savedSearches: an array to pre-populate the savedSearches list
ex. 
``` javascript
    [
      { id: 1536140277368, name: "db admin", selectedSkills: [{name: "mysql", numRatings: 5, competency: {rating: 5, text: "Expert"}}] },
      { id: 1536140277369, name: "fullstack", selectedSkills: [{name: "c#", competency: {rating: 3, text: "Competent"}}, {name: "javascript", competency: {rating: 4, text: "Proficient"}}] }
    ]
```

#### enableSave: sets the save search feature on/off

#### id: sets the id of the search input