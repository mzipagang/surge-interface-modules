import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';

import SkillSelector from '@surge/react-skill-selector';
import UserSelectorFactory from '@surge/react-user-selector';
import TimezoneSelector from '@surge/react-timezone-selector';

/* BEGIN: SkillSelector */

const setSelectedSkills = (skills) => {
  return [
    { name: "mysql", competency: { rating: 4, text: "Proficient" } },
    { name: "c#", competency: { rating: 5, text: "Expert" } },
  ]
}

const getSkills = () => {
  return [
    { name: "mysql", numRatings: 5 },
    { name: "c#", numRatings: 0 },
    { name: "javascript" }
  ];
}

const getCompetencies = () => {
  return [
    { rating: 1, text: "Novice" },
    { rating: 2, text: "Advanced Beginner" },
    { rating: 3, text: "Competent" },
    { rating: 4, text: "Proficient" },
    { rating: 5, text: "Expert" },
    { rating: null, text: "Nice to Have" }
  ];
}

storiesOf('SkillSelector', module)
  .add('default mode', () => (
    <React.Fragment>
      <label htmlFor="testSelector">Skill Selection</label>
      <SkillSelector
        id="testSelector"
        onSelectedSkillsChanged={action('fired onSelectedSkillsChanged')}
        selectedSkills={setSelectedSkills()}
        onSavedSearchesChanged={action('fired onSavedSearchesChanged')}
        savedSearches={[]}
        skills={getSkills()}
        competencyList={getCompetencies()}
        defaultCompetencyIndex={0}
      />
    </React.Fragment>
  ))
  .add('with save feature enabled', () => (
    <React.Fragment>
      <label htmlFor="testSelector">Skill Selection</label>
      <SkillSelector
        id="testSelector"
        onSelectedSkillsChanged={action('fired onSelectedSkillsChanged')}
        selectedSkills={setSelectedSkills()}
        onSavedSearchesChanged={action('fired onSavedSearchesChanged')}
        savedSearches={[]}
        skills={getSkills()}
        competencyList={getCompetencies()}
        defaultCompetencyIndex={2}
        enableSave={true}
      />
    </React.Fragment>
  ));

/* END: SkillSelector */

/* BEGIN: UserSelector */

const Context = React.createContext(null);
const UserSelector = UserSelectorFactory(Context.Consumer);

const fetchUserData = () => Promise.resolve([
  {name: 'George', id: 1},
  {name: 'Blerim', id: 2},
  {name: 'Tyler', id: 3},
  {name: 'Robert', id: 4},
  {name: 'Elon', id: 5},
  {name: 'Lane', id: 6},
  {name: 'Kim', id: 7},
  {name: 'Mark', id: 8}
]);

const fetchProviderUserData = () => Promise.resolve([
  {name: 'George', id: 1},
  {name: 'Ringo', id: 2},
  {name: 'John', id: 3},
  {name: 'Paul', id: 4}
]);

storiesOf('UserSelector', module)
  .add('pass in user data via prop', () => (
    <UserSelector onUserSelected={action('fired onUserSelected')} selectedUser={{id: 2, name: "Ringo"}} fetchUserData={fetchUserData} />
  ))
  .add('pass in user data via Context.Provider', () => (
    <Context.Provider value={fetchProviderUserData}>
      <UserSelector onUserSelected={action('fired onUserSelected')} selectedUser={{id: 1, name: "George"}} fetchUserData={fetchUserData} />
    </Context.Provider>
  ));

/* END: UserSelector */

/* BEGIN: TimezoneSelector */

const selectedTimezone = 'America/New_York';

storiesOf('TimezoneSelector', module)
  .add('use default timezone data (moment/timezone)', () => (
    <TimezoneSelector onTimezoneSelected={action('fired onTimezoneSelected')} selectedTimezone={selectedTimezone} />
  ));

/* END: TimezoneSelector */