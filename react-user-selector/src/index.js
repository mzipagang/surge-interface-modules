import React from "react";
import withUserData from "./components/withUserData.js"
import UsersDropdown from "./components/UsersDropdown.js"

const UserSelectorFactory = (Consumer) => {
  const UserSelector = (props) =>
    <Consumer>
      { value => {
        const fetchFunc = value || props.fetchUserData || (() => []);
        const UsersDropdownWithData = withUserData(fetchFunc, UsersDropdown);
        return <UsersDropdownWithData {...props} />
      }}
    </Consumer>

  return UserSelector;
}

export default UserSelectorFactory;