import React from "react";
import PropTypes from 'prop-types';
import styled from "styled-components";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faChevronDown
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Select from 'react-select';

library.add(faChevronDown);

const Container = styled.div`
  position: relative;
`;

const maxDropdownHeight = 600;

const filterByKeyword = (users, keyword) =>
  users.filter(
    user => (user.name && user.name.toLocaleLowerCase().includes(keyword.toLocaleLowerCase()))
     || (user.email && user.email.toLocaleLowerCase().includes(keyword.toLocaleLowerCase())) 
     || keyword === ""
  );

export default class UsersDropdown extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedUser: props.selectedUser,
      keyword: ""
    };
  }

  onUserSelected = (id) => {
    const user = this.props.users.find(u => id === u.id);
    this.setState({ selectedUser: user });
    this.props.onUserSelected(user);
  }

  enterUser = (e) => {
    this.setState({ keyword: e.target.value });
  }

  render = () => {
    const users = filterByKeyword(this.props.users || [], this.state.keyword)
      .map(user => ({value: user.id, label: user.name}));

    return (
      <Container>
        <Select maxMenuHeight={maxDropdownHeight}
          options={users}
          value={users.find(s => this.state.selectedUser && s.value === this.state.selectedUser.id)}
          onChange={(option) => { this.onUserSelected(option.value) }}
          classNamePrefix="react-select" />
      </Container>
    );
  }
};

UsersDropdown.propTypes = {
  users: PropTypes.array,
  onUserSelected: PropTypes.func,
  selectedUser: PropTypes.object
};
