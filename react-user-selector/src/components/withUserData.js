import React, { Component } from "react";

const withUserData = (fetchFunc, ComposedComponent) => {
  class DataComponent extends Component {
    constructor(props) {
      super(props);

      this.state = {
        data: []
      };
    }
 
    componentDidMount = async () => {
      if(typeof fetchFunc === "function"){
        const data = await fetchFunc();
        this.setState({ data });
      }
    }
 
    render = () => {
      const { data } = this.state;
      return (
        <ComposedComponent users={data} {...this.props} />
      );
    }
  }
  return DataComponent;
}

export default withUserData;