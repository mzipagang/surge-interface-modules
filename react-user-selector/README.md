## Importing

``` javascript
import UserSelectorFactory from '@surge/react-user-selector';
```

## react-user-selector

The plugin can be used like so:

``` javascript
import UserSelectorFactory from '@surge/react-user-selector';

const Context = React.createContext(null);
const UserSelector = UserSelectorFactory(Context.Consumer); // the Context.Consumer parameter allows data to be passed in via a Context.Provider

const onUserSelected = (user) => {
  console.log(user);
}

const fetchUserData = () => Promise.resolve([
  {name: 'George', id: 1},
  {name: 'Ringo', id: 2},
  {name: 'John', id: 3},
  {name: 'Paul', id: 4}
]);

...
render = () => <UserSelector onUserSelected={onUserSelected} selectedUser={selectedUser} fetchUserData={fetchUserData} />
```

The props needed are:

#### onUserSelected: an output function that will be passed the currently selected user

#### fetchUserData: a function that fetches the data for the component and returns an array of users (which needs to match the schema below).
ex.
``` javascript
    [
      {name: 'George Harrison', id: 1},
      {name: 'Ringo Starr', id: 2},
      {name: 'John Lennon', id: 3},
      {name: 'Paul McCartney', id: 4}
    ]
```

#### selectedUser: the pre-selected user when the component is mounted
ex.
``` javascript
    "John Doe"
```

User data can also be passed in via Context.Provider. This overrides the fetchUserData prop, if supplied, of UserSelector.
ex.
``` javascript
const fetchProviderUserData = () => Promise.resolve([
  {name: 'Mick Jagger', id: 1},
  {name: 'Keith Richards', id: 2},
  {name: 'Brian Jones', id: 3},
  {name: 'Ronnie Wood', id: 4}
]);
...
render = () =>
  <Context.Provider value={fetchProviderUserData}>
    <UserSelector onUserSelected={onUserSelected} selectedUser={"John Doe"} fetchUserData={fetchUserData} />
  </Context.Provider>
```