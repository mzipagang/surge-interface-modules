import expect from 'expect';
import React from 'react';
import ReactDOM from 'react-dom';
import ReactTestUtils from 'react-dom/test-utils';
import UserSelectorFactory from '../src';

const Context = React.createContext(null);
const UserSelector = UserSelectorFactory(Context.Consumer);

class Wrapper extends React.PureComponent {
  constructor(props){
    super(props);
  }
  render() {
    return (
      <div>
        <Context.Provider 
          value={this.props.fetchProviderUserData}>
          <UserSelector
            selectedUser={this.props.selectedUser}
            fetchUserData={this.props.fetchUserData} 
          />
        </Context.Provider>
      </div>
    );
  }
}

describe("UserSelector", () => {
  it("renders the pre-selected user in the dropdown", function(done) {  
    this.component = ReactTestUtils.renderIntoDocument(
      <Wrapper
        fetchUserData={() => Promise.resolve([
          {name: 'George', id: 1},
          {name: 'Ringo', id: 2},
          {name: 'John', id: 3},
          {name: 'Paul', id: 4}
        ])}
        selectedUser={{id: 2, name: 'Ringo'}}
      />
    );

    setTimeout(() => {
      const input = ReactTestUtils.findRenderedDOMComponentWithClass(this.component, "react-select__single-value");
      expect(input.innerHTML).toEqual('Ringo');

      done();
    });
  });
});