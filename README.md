## Prerequisites

[Node.js](http://nodejs.org/) >= 6 must be installed.

## Installation

- Running `yarn install` in the component's root directory will install everything you need for storybook.

## Storybook

- `yarn run storybook` will run a development server for storybook [http://localhost:6006](http://localhost:6006) with hot module reloading.